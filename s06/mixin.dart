void main() {
  Equipment equipment = new Equipment();
  equipment.name = 'Equipment-001';
  print(equipment.name);

  Loader loader = new Loader();
  loader.name = 'car-001';
  print(loader.name);
  print(loader.getCategory());
  loader.moveForward(30);

  Car car = new Car();
  car.name = 'car-001';
  print(car.name);
  print(car.getCategory());
  car.moveForward(30);
}

class Equipment {
  String? name;
}

class Loader extends Equipment with Movement {
  String getCategory() {
    return '${this.name} is a loader';
  }
}

class Car with Movement {
  String? name;

  String getCategory() {
    return '${this.name} is a car';
  }
}

mixin Movement {
  num? acceleration;
  void moveForward(num acceleration) {
    print('The vehicle is moving $acceleration forward.');
  }

  void moveBackward() {
    print('The vehicle is moving $acceleration backward.');
  }
}
