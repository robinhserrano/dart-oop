void main() {
  Person person = new Person(firstName: 'John', lastName: 'Smith');
  Employee employee = new Employee(
      lastName: 'Westwoord', firstName: 'Jones', employeeId: 'ID-001');
  print(person.getFullName());
  print(employee.getFullName());
}

class Person {
  String firstName;
  String lastName;
  Person({required this.firstName, required this.lastName});

  String getFullName() {
    return '${this.firstName} ${this.lastName}';
  }
}

class Employee extends Person {
  String employeeId;
  Employee(
      {required String firstName,
      required String lastName,
      required this.employeeId})
      : super(firstName: firstName, lastName: lastName);
}
