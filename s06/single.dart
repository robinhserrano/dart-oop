void main() {
  Person person = new Person(firstName: 'John', lastName: 'Smith');
  print(person.getFullName());
}

class Person {
  String firstName;
  String lastName;
  Person({required this.firstName, required this.lastName});

  String getFullName() {
    return '${this.firstName} ${this.lastName}';
  }
}

class Employee extends Person {
  Employee({firstName, lastName})
      : super(firstName: firstName, lastName: lastName);
}
