void main() {
  List<int> discountRanges = [20, 40, 60, 80];
  List<String> names = ['John', 'Jane', 'Tom'];
  List<String> maritalStatus = ['single', 'married', 'divorced', 'widowed'];

  print(discountRanges);
  print(names);
  print(discountRanges[2]);
  print(names[0]);

  print(discountRanges.length);
  print(names.length);

  names[0] = 'Jonathan';
  print(names);

  names.add('Mark');
  names.insert(0, 'Rosselle');

  maritalStatus.add("");
  print(names.isEmpty);
  print(names.isNotEmpty);
  print(names.first);
  print(names.last);
  print(names.reversed);
  names.sort();
  print(names.reversed);
}
