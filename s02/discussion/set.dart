void main() {
  // A set in Dart is an unordered collection of unique items.
  Set<String> subcontractors = {'Sonderhoff', 'Stahlschmidst'};
  subcontractors.add('Schweisstechnik');
  subcontractors.add('Kreatel');
  subcontractors.add('Kunstoffe');
  subcontractors.remove('Sonderhoff');

  print(subcontractors);
}
