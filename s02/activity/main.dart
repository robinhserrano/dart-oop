void main() {
  Map<String, dynamic> person = new Map();
  person['lastName'] = ["Smith"];
  person['firstName'] = ["John"];
  person['age'] = [34];

  List<String> country = [
    "Japan",
    "Canada",
    "Thailand",
    "Singapore",
    "Philippines",
    "United Kingdom"
  ];

  Set<String> cpu = {
    "Ryzen 3 3200G",
    "Ryzen 5 3600",
    "Ryzen 7 5800X",
    "Ryzen 9 5950X"
  };

  print("Map person elements: $person with a lenght of " +
      person.length.toString());
  print("List country elements: $country with a lenght of " +
      country.length.toString());
  print("Set cpu elements: $cpu with a lenght of " + cpu.length.toString());
}
