void main() {
  List<Equipment> vehicles = [];
  Bulldozer bulldozer1 = new Bulldozer(
    model: "Caterpillar D10",
    bladeType: "U blade"
  );
  TowerCrane towerCrane1 = new TowerCrane(
    model: "370 EC-B 12 Fibre",
    hookRadius: 78,
    maxCapacity: 12000
  );
  Loader loader1 = new Loader(
    model: "Volvo L60H", 
    equipmentType: "wheel loader", 
    tippingLoad: 16530
  );
  vehicles.add(bulldozer1);
  vehicles.add(towerCrane1);
  vehicles.add(loader1);
  vehicles.forEach((element) => print(element.describe()));
}

abstract class Equipment {
  String describe();
}

class Bulldozer implements Equipment {
  String bladeType;
  String model;
  
  Bulldozer({
    required this.bladeType,
    required this.model
    });
  
  String describe() => 'The bulldozer $model has a $bladeType.';
}

class TowerCrane implements Equipment {
  String model;
  num hookRadius;
  num maxCapacity;
  
  TowerCrane({
    required this.model,
    required this.hookRadius,
    required this.maxCapacity
    });
  
  String describe() =>
      'The tower crane $model has a hook radius of $hookRadius and a max capacity of $maxCapacity.';
}

class Loader implements Equipment {
  String model;
  String equipmentType;
  num tippingLoad;
  
  Loader({
    required this.model,
    required this.equipmentType,
    required this.tippingLoad
    });
  
  String describe() =>
      'The loader $model is a $equipmentType and has a tipping load of $tippingLoad lbs.';
}
