import 'worker.dart';

void main() {
  //Person personA = new Person();
  Doctor doctor = new Doctor(firstName: 'John', lastName: 'Smith');
  Carpenter worker = new Carpenter();
  print(worker.getType());
}

abstract class Person {
  String getFullName();
}

//The concrete class is the Doctor class.
class Doctor implements Person {
  @override
  String firstName;
  String lastName;

  Doctor({required this.firstName, required this.lastName});
  //Worker worker = new Worker();

  String getFullName() {
    // TODO: implement getFullName
    return 'Dr. ${this.firstName} ${this.lastName}';
  }
}

class Attorney implements Person {
  String getFullName() {
    // TODO: implement getFullName
    return 'Atty. ';
  }
}
