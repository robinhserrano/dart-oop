void main() {
  //assignment operator
  int x = 1397;
  int y = 7831;

  //arithmetic operator
  num sum = x + y;
  num diff = x + y;
  num product = x + y;
  num quotient = x + y;
  num remainder = 4 + 3;
  num output = (x * y) - (x / y + x);

  //Relational opertor
  bool isGreaterThan = x > y;
  bool isLessThan = x < y;

  bool isGTorThan = x >= y;
  bool isLTorhan = x <= y;

  bool isEqual = x == y;
  bool isNotEqual = x != y;

  //logical operator
  bool isLegalAge = true;
  bool isRegistered = false;

  bool areAllRequirementsMet = isLegalAge && isRegistered;
  bool areSomeRequirementsMet = isLegalAge || isRegistered;
  bool isNotRegistered = !isRegistered;
}
