class Building {
  String _name;
  int floors;
  String address;

  Building(this._name, {required this.floors, required this.address}) {
    print('A building object has been created');
  }

  //The get and set allows for indirect acces to class fields
  String? get Name {
    print('The building\'s name has been retrieved');
    return this._name;
  }

  void set Name(String? name) {
    this._name = name!;
    print('The building\'s name has been changed to $name');
  }

  Map<String, String> getProperties() {
    return {
      'name': this._name,
      'floors': this.floors.toString(),
      'address': this.address
    };
  }
}
