import 'package:flutter/material.dart';
import './answer_button.dart';

class BodyQuestions extends StatelessWidget {
  final questions;
  final int questionIdx;
  final Function nextQuestion;

  BodyQuestions({
      required this.questions, 
      required this.questionIdx, 
      required this.nextQuestion
  });

  @override
  Widget build(BuildContext context) {
      Text questionText = Text(
    questions[questionIdx]['question'].toString(),
    style: TextStyle(fontSize: 24),
);

List<String> options = questions[questionIdx]['options'] as List<String>;

//Before Map['Time Tracking', 'Asset Management', 'Issue Tracking']
//After Map[AnswerButton('Time Tracking'), AnswerButton('Asset Management'), 'AnswerButton(Issue Tracking')]
List<AnswerButton> answerOptions = options.map((String option) {
    return AnswerButton(text: option, nextQuestion: nextQuestion);
}).toList();

return Container(
    width: double.infinity,
    padding: EdgeInsets.all(16),
    child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
            questionText,
            ...answerOptions,
            Container(
                margin: EdgeInsets.only(top: 30),
                width: double.infinity,
                child: ElevatedButton(
                    child: Text('Skip Question'),
                    onPressed: ()=> nextQuestion(null))
            )
        ],
    ),
);
  }
}