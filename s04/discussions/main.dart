void main() {
  print(getCompanyName());
  print(getYearEstablished());
  print(hasOnlineClasses());
  print(getCoordinates());
  print(combineAddress(
      "134 Timog Avenue", "Brgy. Sacred Heart", "Quezon City", "Metro Manila"));
  print(combineName('John', 'Smith'));
  print(combineName('John', 'Smith', isLastNameFirst: true));
  print(combineName('John', 'Smith', isLastNameFirst: false));

  List<String> persons = ['John Doe', 'Jane Doe'];
  List<String> students = ['Nicholas Rush', 'James Holden'];
  persons.forEach((String person) {
    print(persons);
  });

  students.forEach((String person) {
    print(persons);
  });

  //Function as objects AND used as an argument
  //printName{value} is function execution/call/invocation.
  //printName is reference to the given function
  students.forEach(printName);
}

void printName(String name) {
  print(name);
}

String combineName(String firstName, String lastName,
    {bool isLastNameFirst = false}) {
  if (isLastNameFirst) {
    return '$lastName $firstName';
  } else {
    return '$firstName $lastName';
  }
}

String combineAddress(
    String specifics, String barangay, String city, String province) {
  return '$specifics, $barangay, $city, $province';
}

String getCompanyName() {
  return "FFUF";
}

int getYearEstablished() {
  return 2017;
}

bool hasOnlineClasses() {
  return true;
}

//the initial isUnderAge function can be change into a lambda (arrow) function
//A lambda function is a shortcut function for returning values from simple operations.
//the syntax of a lambda function is:
//return-type function-name(parameters) = expression
bool isUnderAge(int age) => (age < 18) ? true : false;

Map<String, double> getCoordinates() {
  return {'latitude': 14.632702, 'longitude': 121.043716};
}
