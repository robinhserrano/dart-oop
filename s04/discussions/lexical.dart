void main() {
  Function discountBy25 = getDiscount(25);
  //print(discountBy25(1400));
  print(getDiscount(50)(1400));
}

Function getDiscount(num percentage) {
  //When the getDiscount is used and the function below is returned,
  //the value of 'percentage' parameter is retained.
  //That is called lexical scope.
  return (num amount) {
    return amount * percentage / 100;
  };
}
