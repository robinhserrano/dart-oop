// Using a function, get the total price of prices [45, 34.2, 176.9, 32.2] and optionally get its discounted price.

// Use a loop to get the total price.

// 288.3 (no discount)
// 230.64 (20% discount)
// 172.98 (40% discount)
// 115.32 (60% discount)
// 57.66 (80% discount)

//still editing
void main() {
  List<double> prices = [45, 34.2, 176.9, 32.2];
  List<double> discounts = [0, 20, 40, 60, 80];
  getTotal(prices, discounts, withDiscounts: true);
}

Function getDiscounts(double percentage) {
  return (double amount) {
    return amount * percentage / 100;
  };
}

void getTotal(List<double> prices, List<double> discounts,
    {bool withDiscounts = false}) {
  double total = 0;
  prices.forEach((item) {
    total += item;
  });
  print("Total Price: " + total.toString());

  if (withDiscounts) {
    for (var i = 0; i < discounts.length; i++) {
      double temp = discounts[i];
      double discounted = total - getDiscounts(temp)(total);
      String text = temp == 0 ? " (no discount)" : " ($temp % discount)";
      print(discounted.toStringAsFixed(2) + '' + text);
    }
  }
}
