void main() {
  List<double> prices = [45, 34.2, 176.9, 32.2];
  print(getTotal(prices));
  print(getDiscounts(getTotal(prices), 80));
} //getDiscounts(<total amount>,<the discount percentage>);

String getDiscounts(double amount, double percent) {
  String discountText =
      percent == 0 ? " (no discount)" : " ($percent % discount)";
  return (amount - (amount * percent / 100)).toStringAsFixed(2) + discountText;
}

double getTotal(List<double> prices) => prices.reduce((a, b) => a + b);
