import './freezed_models/user.dart';

void main(){
    User userA = User(id: 1,email: 'john@gmail.com');
    User userB = User(id: 1,email: 'john@gmail.com');
    print(userA  == userB);
    print(userA.hashCode);
    print(userB.hashCode);

    //Demonstration of object immutability below.
    //Immutability means changes are not allowed.
    //It ensures that an object will not be changed accidentally

    //Instead of directly changing the object's property
    //the object itself will be changed or re-assigned with new value
    //To achieve this, we use the object.copyWith() method

    // print(userA.email);
    // userA = userA.copyWith(email: 'john@hotmail.com');
    // print(userA.email);

    var response = {'id':3, 'email':'doe@gmail.com'};

    User userC = User.fromJson(response);
    print(userC);
    print(userC.toJson());
}