import 'dart:html';

void main() {
  String firstName = 'John';
  String lastName = 'Smith';
  int age = 31;
  double height = 172.45;
  num weight = 64.32;
  bool isRegistered = false;
  List<num> grades = [98.2, 89, 87.88, 91.2];
  Map<String, dynamic> personA = {
    'name': 'Brandon',
    'batch': 213
  }; //Map is an object

  Map<String, dynamic> personB = new Map();
  personB['name'] = 'Juan';
  personB['batch'] = '89';

  final DateTime now = DateTime.now();
  //now = DateTime.now()

  const String companyAcronym = 'FFUF';

  print(firstName + lastName);
  print('Full Name: $firstName $lastName');
  print('Age ' + age.toString());
  print('Height: ' + height.toString());
  print('Weight: ' + weight.toString());
  print('Registered: ' + isRegistered.toString());
  print('Grades: ' + grades.toString());
  print('Current DateTime:' + now.toString());
  print(personB['name']);
}
